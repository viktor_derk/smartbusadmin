<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\Trip;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TripSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Trips');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trip-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Trip'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'id',
                'options' => ['width' => '42']
            ],
            [
                'attribute'=>'id',
                'options' => ['width' => '42']
            ],
            [
                'attribute'=>'from_city',
                'format'=>'text',
                'content'=>function($data){
                    return $data->cityName($data->from_city);
                },
                'filter' => Trip::cityList()
            ],
            'from_date',
            'from_time',
            // 'from_info',
            [
                'attribute'=>'to_city',
                'format'=>'text',
                'content'=>function($data){
                    return $data->cityName($data->to_city);
                },
                'filter' => Trip::cityList()
            ],
            'to_date',
            'to_time',
            // 'to_info',
            // 'info',
            // 'price',
            'bus_id',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {link}',
                'buttons' => [
                    'link' => function ($url,$model,$key) {
                        return Html::a('Бронь('.$model->getReservationsCount().')', Url::to(['reservation/index', 'trip_id' => $model->id]));
                    },
                ],
            ],
        ],
    ]); ?>

</div>
