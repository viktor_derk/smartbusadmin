<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\City;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Trip */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trip-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $cities = City::find()->all();
        $items = ArrayHelper::map($cities,'id','name');
        echo $form->field($model, 'from_city')->dropDownList($items); ?>

    <?= $form->field($model, 'from_date')->widget(DatePicker::classname(), [
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'from_time')->textInput() ?>

    <?= $form->field($model, 'from_info')->textInput(['maxlength' => true]) ?>

    <?php $cities = City::find()->all();
        $items = ArrayHelper::map($cities,'id','name');
        echo $form->field($model, 'to_city')->dropDownList($items); ?>

    <?= $form->field($model, 'to_date')->widget(DatePicker::classname(), [
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'to_time')->textInput() ?>

    <?= $form->field($model, 'to_info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'bus_id')->dropDownList([ '8' => 'На 8 мест', '14' => 'На 14 мест', '15' => 'На 15 мест', '18' => 'На 18 мест', '21' => 'На 21 мест']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
