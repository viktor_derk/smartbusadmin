<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReservationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Reservations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Reservation'), ['create'], ['class' => 'btn btn-success']) ?>
        <?php if(isset($_SESSION['trip_id'])) {
            echo Html::a(Yii::t('app', 'Show All Reservations'), Url::toRoute(['reservation/index', 'trip_id_clear' => '1']), ['class' => 'btn btn-info']);
        }?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'id',
                'options' => ['width' => '42']
            ],
            'trip_id',
            'place',
            'fio',
            'phone',
            'email:email',
            'info1',
            'info2',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
