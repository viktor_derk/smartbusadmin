<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\Trip;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Reservation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reservation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        if(isset($_SESSION['trip_id'])){
            echo $form->field($model, 'trip_id')->textInput()->textInput(['readonly' => true, 'value' => $_SESSION['trip_id']]);
        }else{
            $list = Trip::find()->all();
            $items = ArrayHelper::map($list,'id','id');
            echo $form->field($model, 'trip_id')->dropDownList($items);
        }
    ?>

    <?= $form->field($model, 'place')->textInput() ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'info1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'info2')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
