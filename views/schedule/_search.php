<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScheduleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedule-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'from_city') ?>

    <?= $form->field($model, 'from_time') ?>

    <?= $form->field($model, 'from_info') ?>

    <?= $form->field($model, 'to_city') ?>

    <?php // echo $form->field($model, 'to_time') ?>

    <?php // echo $form->field($model, 'to_info') ?>

    <?php // echo $form->field($model, 'info') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'every_monday') ?>

    <?php // echo $form->field($model, 'every_tuesday') ?>

    <?php // echo $form->field($model, 'every_wednesday') ?>

    <?php // echo $form->field($model, 'every_thursday') ?>

    <?php // echo $form->field($model, 'every_friday') ?>

    <?php // echo $form->field($model, 'every_saturday') ?>

    <?php // echo $form->field($model, 'every_sunday') ?>

    <?php // echo $form->field($model, 'bus_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
