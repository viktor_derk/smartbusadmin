<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Schedule */

$title='Auto Create Trips';
$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Schedules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
?>
<div class="schedule-view">

    <h1><?= Html::encode($title) ?></h1>

    <?php
        echo Html::dropDownList('monthList', null, ['01' => '01', '02' => '02', '03' => '03', '04' => '04',
            '05' => '05', '06' => '06', '07' => '07', '08' => '08', '09' => '09', '10' => '10', '11' => '11', '12' => '12'],
            [   'prompt' => 'select month',
                'onchange'=> 'monthListOnchange()'
            ]);?>

    <br>
    <br>

    <?php
        echo Html::dropDownList('yearList', null, ['2016' => '2016', '2017' => '2017', '2018' => '2018', '2019' => '2019',
             '2020' => '2020', '2021' => '2021', '2022' => '2022'],
            [   'prompt' => 'select year',
                'onchange'=> 'yearListOnchange()'
            ]);
    ?>

    <br>
    <br>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create-trips', 'schedule_id' => '', 'month' => '', 'year' => ''], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to create this schedules?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <script>
        function monthListOnchange() {
            var monthList = document.getElementsByName("monthList")[0];
            var btn = document.getElementsByClassName("btn")[0];
            var url = updateQueryStringParameter(btn.getAttribute("href"), "month", monthList.value);
            btn.setAttribute("href", url);
            console.log(btn);
        }
        function yearListOnchange() {
            var yearList = document.getElementsByName("yearList")[0];
            var btn = document.getElementsByClassName("btn")[0];
            var url = updateQueryStringParameter(btn.getAttribute("href"), "year", yearList.value);
            btn.setAttribute("href", url);
            console.log(btn);
        }
        function updateQueryStringParameter(uri, key, value) {
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                return uri + separator + key + "=" + value;
            }
        }
    </script>

</div>
