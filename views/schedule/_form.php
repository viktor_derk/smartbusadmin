<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\City;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Schedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $cities = City::find()->all();
        $items = ArrayHelper::map($cities,'id','name');
        echo $form->field($model, 'from_city')->dropDownList($items); ?>

    <?= $form->field($model, 'from_time')->textInput() ?>

    <?= $form->field($model, 'from_info')->textInput(['maxlength' => true]) ?>

    <?php $cities = City::find()->all();
        $items = ArrayHelper::map($cities,'id','name');
        echo $form->field($model, 'to_city')->dropDownList($items); ?>

    <?= $form->field($model, 'to_time')->textInput() ?>

    <?= $form->field($model, 'to_info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'every_monday')->dropDownList([ '0' => 'Нет', '1' => 'Да', ]); ?>

    <?= $form->field($model, 'every_tuesday')->dropDownList([ '0' => 'Нет', '1' => 'Да', ]); ?>

    <?= $form->field($model, 'every_wednesday')->dropDownList([ '0' => 'Нет', '1' => 'Да', ]); ?>

    <?= $form->field($model, 'every_thursday')->dropDownList([ '0' => 'Нет', '1' => 'Да', ]); ?>

    <?= $form->field($model, 'every_friday')->dropDownList([ '0' => 'Нет', '1' => 'Да', ]); ?>

    <?= $form->field($model, 'every_saturday')->dropDownList([ '0' => 'Нет', '1' => 'Да', ]); ?>

    <?= $form->field($model, 'every_sunday')->dropDownList([ '0' => 'Нет', '1' => 'Да', ]); ?>

    <?= $form->field($model, 'bus_id')->dropDownList([ '8' => 'На 8 мест', '14' => 'На 14 мест', '15' => 'На 15 мест', '18' => 'На 18 мест', '21' => 'На 21 мест', ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
