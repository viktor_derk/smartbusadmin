<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Schedule;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// Note: http://nix-tips.ru/yii2-razbiraemsya-s-gridview.html

$this->title = Yii::t('app', 'Schedules');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Schedule'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Auto Create Trips'), ['trips'], ['class' => 'btn btn-danger']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'id',
                'options' => ['width' => '42']
            ],
            [
                'attribute'=>'from_city',
                'format'=>'text',
                'content'=>function($data){
                    return $data->cityName($data->from_city);
                },
                'filter' => Schedule::cityList()
            ],
            'from_time',
            'from_info',
            [
                'attribute'=>'to_city',
                'format'=>'text',
                'content'=>function($data){
                    return $data->cityName($data->to_city);
                },
                'filter' => Schedule::cityList()
            ],
            'to_time',
            'to_info',
            // 'info',
            // 'price',
            // 'every_monday',
            // 'every_tuesday',
            // 'every_wednesday',
            // 'every_thursday',
            // 'every_friday',
            // 'every_saturday',
            // 'every_sunday',
            'bus_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
