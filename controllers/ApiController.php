<?php

namespace app\controllers;

use app\models\Reservation;
use DateInterval;
use DateTime;
use Yii;
use app\models\Schedule;
use app\models\City;
use app\models\Trip;
use yii\web\Response;
use yii\web\Controller;
use yii\db\Expression;

class ApiController extends Controller
{
    // Json format notes: https://github.com/samdark/yii2-cookbook/blob/master/book/response-formats.md

    // http://localhost/smartbus/web/index.php/api/schedules
    // http://admin.hochyehat.com/web/index.php/api/schedules
    public function actionSchedules()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = Schedule::find()->all();
        foreach($list as $item){
            $item->from_city = City::findOne($item->from_city);
            $item->to_city = City::findOne($item->to_city);
        }
        $result = array (
            "success" => true,
            "data" => $list,
        );
        return $result;
    }

    // http://localhost/smartbus/web/index.php/api/cities
    // http://admin.hochyehat.com/web/index.php/api/cities
    public function actionCities()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = City::find()->all();
        $result = array (
            "success" => true,
            "data" => $list,
        );
        return $result;
    }

    public function actionCitiesCached()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $headers = Yii::$app->request->headers;
        $IfModifiedSince = $headers->get('If-Modified-Since');
        //$rnd = rand(0,1) == 1;
        if($IfModifiedSince) {
            $format = 'Y-m-d H:i:s';
            $IfModifiedSinceDate = DateTime::createFromFormat($format, $IfModifiedSince);
            if($IfModifiedSinceDate > (new DateTime("now"))->sub(new DateInterval('PT5M'))){
                Yii::$app->response->statusCode = 304;
                return;
                die();
            }
        }
        
        //if($rnd){
            $list = City::find()->orderBy(new Expression('rand()'))->all();
        //}else{
        //    $list = City::find()->limit(4)->orderBy(new Expression('rand()'))->all();
        //}

        $result = array(
            "success" => true,
            "data" => $list,
        );

        return $result;
    }

    // Date compare notes: http://stackoverflow.com/questions/31096787/using-yii2-date-range-in-active-record

    // http://localhost/smartbus/web/index.php/api/trips?from_date=2016-01-01&to_date=2016-03-01
    // http://admin.hochyehat.com/web/index.php/api/trips?from_date=2016-01-01&to_date=2016-03-01
    public function actionTrips($from_date=null,$to_date=null,$from_city=null,$to_city=null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = array();
        if(!isset($from_date)||!isset($to_date)){
            $result = array (
                "success" => false,
                "message" => 'Not all parameters set!',
            );
            return $result;
        }
        if(isset($from_city) && isset($to_city)){
            $list = Trip::find()
                ->where('from_date >= "' . $from_date . '"')
                ->andWhere('to_date <= "' . $to_date . '"')
                ->andWhere(['from_city' => $from_city])
                ->andWhere(['to_city' => $to_city])
                ->all();
        }else {
            $list = Trip::find()
                ->where('from_date >= "' . $from_date . '"')
                ->andWhere('to_date <= "' . $to_date . '"')
                ->all();
        }
        $data = array ();
        foreach($list as $item){
            $data [] = array (
                "id" => $item->id,
                "from_city" => City::findOne($item->from_city),
                "from_date" => $item->from_date,
                "from_time" => $item->from_time,
                "from_info" => $item->from_info,
                "to_city" => City::findOne($item->to_city),
                "to_date" => $item->to_date,
                "to_time" => $item->to_time,
                "to_info" => $item->to_info,
                "info" => $item->info,
                "price" => $item->price,
                "bus_id" => $item->bus_id,
                "reservation_count" => $item->getReservationsCount(),
            );
        }
        $result = array (
            "success" => true,
            "data" => $data,
        );
        return $result;
    }

    // http://localhost/smartbus/web/index.php/api/trip?id=4
    // http://admin.hochyehat.com/web/index.php/api/trip?id=16
    public function actionTrip($id=null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = array();
        if(!isset($id)){
            $result = array (
                "success" => false,
                "message" => 'Trip id is not set!',
            );
            return $result;
        }
        $item = Trip::findOne($id);
        if(!isset($item)){
            $result = array (
                "success" => false,
                "message" => 'Рейс не найден!',
            );
            return $result;
        }
        $data = array (
            "id" => $item->id,
            "from_city" => City::findOne($item->from_city),
            "from_date" => $item->from_date,
            "from_time" => $item->from_time,
            "from_info" => $item->from_info,
            "to_city" => City::findOne($item->to_city),
            "to_date" => $item->to_date,
            "to_time" => $item->to_time,
            "to_info" => $item->to_info,
            "info" => $item->info,
            "price" => $item->price,
            "bus_id" => $item->bus_id,
            "reservations" => $item->getAllReservations(),
        );
        $result = array (
            "success" => true,
            "data" => $data,
        );
        return $result;
    }

    // http://localhost/smartbus/web/index.php/api/create-trip?from_city=6&from_date=2016-01-01&from_time=11:00:00&from_info=1&to_city=7&to_date=2016-01-02&to_time=12:00:00&to_info=2&info=blabla&price=555&bus_id=14
    // http://admin.hochyehat.com/web/index.php/api/create-trip?from_city=6&from_date=2016-01-01&from_time=11:00:00&from_info=1&to_city=7&to_date=2016-01-02&to_time=12:00:00&to_info=2&info=blabla&price=555&bus_id=14
    public function actionCreateTrip($from_city=null,$from_date=null,$from_time=null,$from_info=null,$to_city=null,$to_date=null,
                                     $to_time=null,$to_info=null,$info=null,$price=null,$bus_id=null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = array();
        if(!isset($from_city)||!isset($from_date)||!isset($from_time)||!isset($from_info)||!isset($to_city)||
            !isset($to_date)||!isset($to_time)||!isset($to_info)||!isset($info)||!isset($price)||!isset($bus_id)){
            $result = array (
                "success" => false,
                "message" => 'Not all parameters set!',
            );
            return $result;
        }

        $item = new Trip();
        $item->from_city = $from_city;
        $item->from_date = $from_date;
        $item->from_time = $from_time;
        $item->from_info = $from_info;
        $item->to_city = $to_city;
        $item->to_date = $to_date;
        $item->to_time = $to_time;
        $item->to_info = $to_info;
        $item->info = $info;
        $item->price = $price;
        $item->bus_id = $bus_id;

        if($item->save()){
            $result = array(
                "success" => true,
                "data" => Trip::findOne($item->id)
            );
        }else{
            $result = array(
                "success" => false,
                "message" => $item->errors,
            );
        }
        return $result;
    }

    // http://localhost/smartbus/web/index.php/api/update-trip?id=16&from_city=7&from_date=2016-01-01&from_time=11:00:00&from_info=1&to_city=6&to_date=2016-01-02&to_time=12:00:00&to_info=2&info=blabla&price=556&bus_id=15
    public function actionUpdateTrip($id=null,$from_city=null,$from_date=null,$from_time=null,$from_info=null,$to_city=null,
                                     $to_date=null,$to_time=null,$to_info=null,$info=null,$price=null,$bus_id=null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = array();
        if(!isset($id)||!isset($from_city)||!isset($from_date)||!isset($from_time)||!isset($from_info)||
            !isset($to_city)||!isset($to_date)||!isset($to_time)||!isset($to_info)||!isset($info)||!isset($price)||
            !isset($bus_id)){
            $result = array (
                "success" => false,
                "message" => 'Not all parameters set!',
            );
            return $result;
        }

        $item=Trip::findOne($id);
        if(!isset($item)){
            $result = array (
                "success" => false,
                "message" => 'Рейс не найден!',
            );
            return $result;
        }

        $item->from_city = $from_city;
        $item->from_date = $from_date;
        $item->from_time = $from_time;
        $item->from_info = $from_info;
        $item->to_city = $to_city;
        $item->to_date = $to_date;
        $item->to_time = $to_time;
        $item->to_info = $to_info;
        $item->info = $info;
        $item->price = $price;
        $item->bus_id = $bus_id;

        if($item->save()){
            $result = array(
                "success" => true,
                "data" => Trip::findOne($item->id)
            );
        }else{
            $result = array(
                "success" => false,
                "message" => $item->errors,
            );
        }
        return $result;
    }

    // http://localhost/smartbus/web/index.php/api/delete-trip?id=4
    // http://admin.hochyehat.com/web/index.php/api/delete-trip?id=18
    public function actionDeleteTrip($id=null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = array();
        if(!isset($id)){
            $result = array (
                "success" => false,
                "message" => 'Trip id is not set!',
            );
            return $result;
        }
        $item=Trip::findOne($id);
        if(!isset($item)){
            $result = array (
                "success" => false,
                "message" => 'Рейс не найден!',
            );
            return $result;
        }
        if ($item->getReservationsCount() > 0) {
            $result = array(
                "success" => false,
                "message" => 'Рейс содержит бронированные места!',
            );
        } else {
            if ($item->delete()) {
                $result = array(
                    "success" => true,
                );
            } else {
                $result = array(
                    "success" => false,
                    "message" => $item->errors,
                );
            }
        }
        return $result;
    }

    // http://localhost/smartbus/web/index.php/api/create-reservation?trip_id=4&place=10&fio=12333&phone=12333&email=test@test.com
    // http://admin.hochyehat.com/web/index.php/api/create-reservation?trip_id=20&place=10&fio=12333&phone=12333&email=test@test.com
    public function actionCreateReservation($trip_id=null,$place=null,$fio=null,$phone=null,$email=null,$info1=null,$info2=null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = array();
        if(!isset($trip_id)||!isset($place)||!isset($fio)||!isset($phone)){
            $result = array (
                "success" => false,
                "message" => 'Not all parameters set!',
            );
            return $result;
        }

        $item = new Reservation();
        $item->trip_id = $trip_id;
        $item->place = $place;
        $item->fio = $fio;
        $item->phone = $phone;
        $item->email = $email;
        $item->info1 = $info1;
        $item->info2 = $info2;

        if($item->save()){
            $result = array(
                "success" => true,
                "data" => Reservation::findOne($item->id)
            );
        }else{
            $result = array(
                "success" => false,
                "message" => $item->errors,
            );
        }
        return $result;
    }

    public function actionUpdateReservation($id=null,$trip_id=null,$place=null,$fio=null,$phone=null,$email=null,$info1=null,$info2=null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = array();
        if(!isset($id)||!isset($trip_id)||!isset($place)||!isset($fio)||!isset($phone)){
            $result = array (
                "success" => false,
                "message" => 'Not all parameters set!',
            );
            return $result;
        }

        $item = Reservation::findOne($id);
        if(!isset($item)){
            $result = array (
                "success" => false,
                "message" => 'Бронь не найдена!',
            );
            return $result;
        }

        $item->trip_id = $trip_id;
        $item->place = $place;
        $item->fio = $fio;
        $item->phone = $phone;
        if(!isset($email)){
            $item->email = $email;
        }
        if(!isset($info1)){
            $item->info1 = $info1;
        }
        if(!isset($info2)){
            $item->info2 = $info2;
        }

        $item->update();

        if(count($item->errors)>0){
            $result = array(
                "success" => false,
                "message" => $item->errors,
            );
        }else {
            $result = array(
                "success" => true,
                "data" => Reservation::findOne($item->id)
            );
        }
        return $result;
    }

    // http://localhost/smartbus/web/index.php/api/reservation?id=5
    // http://admin.hochyehat.com/web/index.php/api/reservation?id=18
    public function actionReservation($id=null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = array();
        if(!isset($id)){
            $result = array (
                "success" => false,
                "message" => 'Reservation id is not set!',
            );
            return $result;
        }
        $item = Reservation::findOne($id);
        if(!isset($item)){
            $result = array (
                "success" => false,
                "message" => 'Бронь не найдена!',
            );
            return $result;
        }
        $result = array (
            "success" => true,
            "data" => $item,
        );
        return $result;
    }

    // http://localhost/smartbus/web/index.php/api/delete-reservation?id=9
    // http://admin.hochyehat.com/web/index.php/api/delete-reservation?id=18
    public function actionDeleteReservation($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = array();
        if(!isset($id)){
            $result = array (
                "success" => false,
                "message" => 'Reservation id is not set!',
            );
            return $result;
        }

        $item = Reservation::findOne($id);
        if(!isset($item)){
            $result = array (
                "success" => false,
                "message" => 'Бронь не найдена!',
            );
            return $result;
        }

        if ($item->delete()) {
            $result = array(
                "success" => true,
            );
        } else {
            $result = array(
                "success" => false,
                "message" => $item->errors,
            );
        }
        return $result;
    }

    // http://localhost/smartbus/web/index.php/api/create-month-trips?month=03&year=2016
    public function actionCreateMonthTrips($schedule_id=null,$month=null,$year=null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = array();
        if(!isset($month)||!isset($year)){
            $result = array (
                "success" => false,
                "message" => 'Not all parameters set!',
            );
            return $result;
        }
        //$date = strtotime("01-".$month."-".$year);
        //if($date<time()){
        //    $result = array (
        //        "success" => false,
        //        "message" => 'Past date!',
        //    );
        //    return $result;
        //}
        Schedule::createMonthTrips($schedule_id,$month,$year);
        $result = array (
            "success" => true,
            //"data" => ...,
        );
        return $result;
    }
}
