<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Schedule;

/**
 * ScheduleSearch represents the model behind the search form about `app\models\Schedule`.
 */
class ScheduleSearch extends Schedule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'from_city', 'to_city', 'every_monday', 'every_tuesday', 'every_wednesday', 'every_thursday', 'every_friday', 'every_saturday', 'every_sunday', 'bus_id'], 'integer'],
            [['from_time', 'from_info', 'to_time', 'to_info', 'info'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Schedule::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'from_city' => $this->from_city,
            'from_time' => $this->from_time,
            'to_city' => $this->to_city,
            'to_time' => $this->to_time,
            'price' => $this->price,
            'every_monday' => $this->every_monday,
            'every_tuesday' => $this->every_tuesday,
            'every_wednesday' => $this->every_wednesday,
            'every_thursday' => $this->every_thursday,
            'every_friday' => $this->every_friday,
            'every_saturday' => $this->every_saturday,
            'every_sunday' => $this->every_sunday,
            'bus_id' => $this->bus_id,
        ]);

        $query->andFilterWhere(['like', 'from_info', $this->from_info])
            ->andFilterWhere(['like', 'to_info', $this->to_info])
            ->andFilterWhere(['like', 'info', $this->info]);

        return $dataProvider;
    }
}
