<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Trip;

/**
 * TripSearch represents the model behind the search form about `app\models\Trip`.
 */
class TripSearch extends Trip
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'from_city', 'to_city', 'bus_id'], 'integer'],
            [['from_date', 'from_time', 'from_info', 'to_date', 'to_time', 'to_info', 'info'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trip::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'from_city' => $this->from_city,
            'from_date' => $this->from_date,
            'from_time' => $this->from_time,
            'to_city' => $this->to_city,
            'to_date' => $this->to_date,
            'to_time' => $this->to_time,
            'price' => $this->price,
            'bus_id' => $this->bus_id,
        ]);

        $query->andFilterWhere(['like', 'from_info', $this->from_info])
            ->andFilterWhere(['like', 'to_info', $this->to_info])
            ->andFilterWhere(['like', 'info', $this->info]);

        return $dataProvider;
    }
}
