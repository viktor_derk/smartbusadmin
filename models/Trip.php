<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%trip}}".
 *
 * @property integer $id
 * @property integer $from_city
 * @property string $from_date
 * @property string $from_time
 * @property string $from_info
 * @property integer $to_city
 * @property string $to_date
 * @property string $to_time
 * @property string $to_info
 * @property string $info
 * @property double $price
 * @property integer $bus_id
 */
class Trip extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%trip}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_city', 'from_date', 'from_time', 'from_info', 'to_city', 'to_date', 'to_time', 'to_info', 'info', 'price', 'bus_id'], 'required'],
            [['from_city', 'to_city', 'bus_id', ], 'integer'],
            [['from_date', 'from_time', 'to_date', 'to_time', ], 'safe'],
            [['price'], 'number'],
            [['from_info', 'to_info', 'info'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '#'),
            'from_city' => Yii::t('app', 'Город отправления'),
            'from_date' => Yii::t('app', 'Дата отправления'),
            'from_time' => Yii::t('app', 'Время отправления'),
            'from_info' => Yii::t('app', 'Место отправления '),
            'to_city' => Yii::t('app', 'Город прибытия'),
            'to_date' => Yii::t('app', 'Дата прибытия'),
            'to_time' => Yii::t('app', 'Время прибытия'),
            'to_info' => Yii::t('app', 'Место прибытия'),
            'info' => Yii::t('app', 'Детальное описание'),
            'price' => Yii::t('app', 'Цена'),
            'bus_id' => Yii::t('app', '# автобуса'),
        ];
    }

    public function cityName($city_id) {
        $city = City::find()
            ->where(['id' => $city_id])
            ->one();
        if (isset( $city )) {
            return $city->name;
        } else {
            return "";
        }
    }

    public static function cityList() {
        $cities = City::find()->all();
        return ArrayHelper::map($cities, 'id', 'name');
    }

    public function getAllReservations()
    {
        $list = Reservation::find()
            ->where(['trip_id' => $this->id])
            ->all();
        return $list;
    }

    public function getReservationsCount() {
        $list = $this->getAllReservations();
        if($list){
            return count($list);
        }else{
            return 0;
        }
    }

    /**
     * @inheritdoc
     * @return TripQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TripQuery(get_called_class());
    }
}
