<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%reservation}}".
 *
 * @property integer $id
 * @property integer $trip_id
 * @property integer $place
 * @property string $fio
 * @property string $phone
 * @property string $email
 * @property string $info1
 * @property string $info2
 */
class Reservation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reservation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trip_id', 'place', 'fio', 'phone'], 'required'],
            [['trip_id', 'place'], 'integer'],
            [['fio', 'phone', 'email', 'info1', 'info2'], 'string', 'max' => 256],
            ['email', 'email'],
            ['place', 'validatePlace'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '#'),
            'trip_id' => Yii::t('app', '# поездки'),
            'place' => Yii::t('app', 'Место'),
            'fio' => Yii::t('app', 'ФИО'),
            'phone' => Yii::t('app', 'Телефон'),
            'email' => Yii::t('app', 'Email'),
            'info1' => Yii::t('app', 'Доп. 1'),
            'info2' => Yii::t('app', 'Доп. 2'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReservationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReservationQuery(get_called_class());
    }

    public function validatePlace($attribute, $params)
    {
        $value = $this->$attribute;
        if($value<0){
            $this->addError($attribute, 'Номер места не может быть меньше 0, причем 0 это резервное место');
            return;
        }
        $trip = Trip::findOne($this->trip_id);
        if(!isset($trip)){
            $this->addError($attribute, 'Рейс, на который производится бронь - удален!');
            return;
        }
        if($value>$trip->bus_id){
            $this->addError($attribute, 'В этом рейсе максимальное количество мест '.$trip->bus_id);
            return;
        }
        if($value==0){
            return;
        }
        $reservation = Reservation::find()
            ->where(['trip_id' => $this->trip_id])
            ->andWhere(['place' => $value])
            ->one();
        if($this->scenario=='create'){
            if (isset($reservation)) {
                $this->addError($attribute, 'Это место уже забронировано!');
                return;
            }
        }else{
            if (isset($reservation) && $reservation->id!=$this->id) {
                $this->addError($attribute, 'Это место уже забронировано!');
                return;
            }
        }
    }
}
