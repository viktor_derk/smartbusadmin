<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%schedule}}".
 *
 * @property integer $id
 * @property integer $from_city
 * @property string $from_time
 * @property string $from_info
 * @property integer $to_city
 * @property string $to_time
 * @property string $to_info
 * @property string $info
 * @property double $price
 * @property integer $every_monday
 * @property integer $every_tuesday
 * @property integer $every_wednesday
 * @property integer $every_thursday
 * @property integer $every_friday
 * @property integer $every_saturday
 * @property integer $every_sunday
 * @property integer $bus_id
 */
class Schedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%schedule}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'from_city', 'from_time', 'from_info', 'to_city', 'to_time', 'to_info', 'info', 'price', 'every_monday', 'every_tuesday', 'every_wednesday', 'every_thursday', 'every_friday', 'every_saturday', 'every_sunday', 'bus_id'], 'required'],
            [['id', 'from_city', 'to_city', 'every_monday', 'every_tuesday', 'every_wednesday', 'every_thursday', 'every_friday', 'every_saturday', 'every_sunday', 'bus_id'], 'integer'],
            ['bus_id', 'in', 'range'=>['8', '14', '15', '18', '21']],
            [['from_time', 'to_time'], 'safe'],
            [['price'], 'number'],
            [['from_info', 'to_info', 'info'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '#'),
            'from_city' => Yii::t('app', 'Город отправления'),
            'from_time' => Yii::t('app', 'Время отправления'),
            'from_info' => Yii::t('app', 'Место отправления '),
            'to_city' => Yii::t('app', 'Город прибытия'),
            'to_time' => Yii::t('app', 'Время прибытия'),
            'to_info' => Yii::t('app', 'Место прибытия'),
            'info' => Yii::t('app', 'Детальное описание'),
            'price' => Yii::t('app', 'Цена'),
            'every_monday' => Yii::t('app', 'Каждый понедельник'),
            'every_tuesday' => Yii::t('app', 'Каждый вторник'),
            'every_wednesday' => Yii::t('app', 'Каждую среду'),
            'every_thursday' => Yii::t('app', 'Каждый четверг'),
            'every_friday' => Yii::t('app', 'Каждую пятницу'),
            'every_saturday' => Yii::t('app', 'Каждую субботу'),
            'every_sunday' => Yii::t('app', 'Каждое воскресенье '),
            'bus_id' => Yii::t('app', '# автобуса'),
        ];
    }

    public function cityName($city_id) {
        $city = City::find()
            ->where(['id' => $city_id])
            ->one();
        if (isset( $city )) {
            return $city->name;
        } else {
            return "";
        }
    }

    public static function cityList() {
        $cities = City::find()->all();
        return ArrayHelper::map($cities, 'id', 'name');
    }

    /**
     * @inheritdoc
     * @return ScheduleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ScheduleQuery(get_called_class());
    }

    public function isActiveOnWeekDay($weekday){
        if($weekday==1 && $this->every_monday>0){
            return TRUE;
        }
        if($weekday==2 && $this->every_tuesday>0){
            return TRUE;
        }
        if($weekday==3 && $this->every_wednesday>0){
            return TRUE;
        }
        if($weekday==4 && $this->every_thursday>0){
            return TRUE;
        }
        if($weekday==5 && $this->every_friday>0){
            return TRUE;
        }
        if($weekday==6 && $this->every_saturday>0){
            return TRUE;
        }
        if($weekday==7 && $this->every_sunday>0){
            return TRUE;
        }
        return FALSE;
    }

    public static function createMonthTrips($schedule_id,$month,$year){
        if(!isset($schedule_id)||$schedule_id==null||$schedule_id==''){
            $schedules = Schedule::find()->all();
        }else{
            $schedules = Schedule::findAll($schedule_id);
        }
        foreach($schedules as $schedule){
            //get list all days in month in year
            $start_date = "01-".$month."-".$year;
            $start_time = strtotime($start_date);
            $end_time = strtotime("+1 month", $start_time);
            $days = array();
            for($i=$start_time; $i<$end_time; $i+=86400)
            {
                // test all days to schedule item
                $date = date('Y-m-d', $i);
                $weekday = date('N', $i);
                if($schedule->isActiveOnWeekDay($weekday)) {
                    $days[] = $date;
                }
            }
            foreach($days as $day){
                $trip = new Trip();
                $trip->from_city = $schedule->from_city;
                $trip->from_date = $day;
                $trip->from_time = $schedule->from_time;
                $trip->from_info = $schedule->from_info;
                $trip->to_city = $schedule->to_city;
                $trip->to_date = $day;
                $trip->to_time = $schedule->to_time;
                $trip->to_info = $schedule->to_info;
                $trip->info = $schedule->info;
                $trip->price = $schedule->price;
                $trip->bus_id = $schedule->bus_id;
                $trip->save(false);
            }
        }
    }
}
